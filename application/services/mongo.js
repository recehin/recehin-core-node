import mongoose from 'mongoose'
import config from '../../config/config'

class MongoDB{
    constructor(){
        this.mongoDB = {};
        this.url = 'mongodb://'+config.mongodb.host+'/'+config.mongodb.database;
    }
    initialize(){
        this.mongoDB = mongoose.createConnection(this.url, function(err){
            if(err) throw err
            console.log("MongoDB Connected")
        });
    }
}

module.exports = new MongoDB();