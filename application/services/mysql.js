import Sequelize from 'sequelize';
import config from '../../config/config'

class MySQL{
    constructor(){
        this.SQL = {}
    }

    initialize(){
        this.SQL = new Sequelize(config.mysql.database, config.mysql.user, config.mysql.password, {
            host: config.mysql.host,
            dialect: 'mysql',
            pool: { max: 5, min: 0, acquire: 30000, idle: 10000 },
          });
        this.SQL.authenticate().then(()=>{
            console.log("MySQL Connected")
        }).catch(err =>{
            console.error("Unable connect to Database", err)
        })
    }

    query(query){
        this.SQL.query(query).spread((result,metadata)=>{console.log(metadata, result)})
    }
}

module.exports = new MySQL();