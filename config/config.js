const ENV = process.env.NODE_ENV

const Config = {
    DEV: {
		url: {
			host: '0.0.0.0',
			port: '7001'
		},
		redis: {
			host: '127.0.0.1',
			port: '6379',
			db: 1
		},
		mysql:{
			host: "localhost",
			user: "root",
			password: "",
			database: "recehin"
		},
		mongodb:{
			host:"localhost",
			user:"root",
			password:"",
			database:"recehin_mongo"
		}
    },
	STAGING: {
		url: {
			host: '0.0.0.0',
			port: '7011'
		},
		redis: {
			host: '127.0.0.1',
			port: '6379',
			db: 1
		},
		mysql:{
			host: "localhost",
			user: "root",
			password: "",
			database: "recehin"
		},
		mongodb:{
			host:"localhost",
			user:"root",
			password:"",
			database:"recehin_mongo"
		},
    },
    PROD: {
		url: {
			host: '0.0.0.0',
			port: '9005'
		},
		redis: {
			host: '127.0.0.1',
			port: '7111',
			db: 1
		},
		mysql:{
			host: "localhost",
			user: "root",
			password: "",
			database: "recehin"
		},
		mongodb:{
			host:"localhost",
			user:"root",
			password:"",
			database:"recehin_mongo"
		}
	},
};

const activeConfig = Config[ENV]

export { activeConfig as default };