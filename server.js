import express from 'express';
import Pipa from 'pipa';
import bodyParser from 'body-parser'
import expressLogger from 'express-request-logger'
import Winston from 'winston'
import config from './config/config'
import MySQL from './application/services/mysql'
import MongoDB from './application/services/mongo'



  
const app = express();

let log = new (Winston.Logger)({
    transports: [
      new (Winston.transports.Console)({ colorize: true }),
      new (Winston.transports.File)({ filename: `${__dirname}/logs/appsLog.log`, handleExceptions: true, colorize: true })
    ],
    exceptionHandlers: [
      new (Winston.transports.Console)({ colorize: true }),
      new (Winston.transports.File)({ filename: `${__dirname}/logs/exceptionsLog.log`, handleExceptions: true, colorize: true })
    ]
  });

MongoDB.initialize();
MySQL.initialize();

app.use((req,res,next)=> {req.log = log; next()})
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(expressLogger.create(log));


let pipa = new Pipa(app, 'application/routes', 'application/controllers');

pipa.open()

app.listen(config.url.port)

console.log("-------------------------------")
console.log("RECEHIN CORE NODE")
console.log("SERVER RUNNING ON PORT : " + config.url.port)
console.log("-------------------------------")